package com.example.nizamovnr.moxy

import android.os.Bundle
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.nizamovnr.moxy.interfaces.MainView
import com.example.nizamovnr.moxy.interfaces.`MainView$$State`
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseView<MainPresenter>(), MainView {

    override fun buildPresenter(): MainPresenter {
        return this.presenter

    }

    @InjectPresenter
    lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener { presenter.onPrintHuiClick() }
        printDibil()
    }

    override fun printHui() {
        text.text = "Hui"
    }
}
