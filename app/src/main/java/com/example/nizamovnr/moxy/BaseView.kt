package com.example.nizamovnr.moxy

import com.arellomobile.mvp.MvpAppCompatActivity
import com.arellomobile.mvp.MvpPresenter
import com.arellomobile.mvp.MvpView

abstract class BaseView<T : MvpPresenter<out MvpView>>: MvpAppCompatActivity(){

    fun printDibil() {
        println(buildPresenter())
    }

    abstract fun buildPresenter(): T

}