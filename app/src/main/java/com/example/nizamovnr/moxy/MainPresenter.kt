package com.example.nizamovnr.moxy

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.nizamovnr.moxy.interfaces.MainView

@InjectViewState
class MainPresenter: MvpPresenter<MainView>(){

    fun printDibil() {
        println("Nurshat dibil")
    }

    fun onPrintHuiClick(){
        viewState.printHui()
    }

    override fun toString(): String {
        return "YOPTA"
    }
}